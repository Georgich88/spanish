# Jorge Isaev

## ¿Cómo te llamas?

Me llamo Jorge. Mi apellido es Isaev.

## ¿Cuántos años tienes?

Tengo treinta y cuatro años

## ¿De dónde eres?

Soy de Rusia, pero ahora vivo en Argentina.  
Soy de la tierra del mejor vodka y de Yuriy Gagarin.  
Vivo con mi mujer.

## ¿Hablas español?

Hablo inglés, ruso y español.

## ¿A qué te dedicas?

Trabajo como programador.  
No canto canciones, no bailo, pero viajo mucho, porque trabajo en la oficina remota.   
Mi mujer también es programadora.

## Educación

Siempre aprendo porque soy programador.  
Mi mujer y yo aprendemos Español juntas.

## Deporte

Aprendo jugar tenis y voy al gimnasio a hacer ejercicio.
No corro por la mañana.

## Comida y bebida

Como muchas frutas y verduras, no fumo.
Los fines de semana me gusta comer asada carne y beber vino o cerveza. Estoy feliz y vio positivamente.
Nosotros a menudo vamos a restaurantes los
viernes y sábados. Nosotros no dividimos log gastos, yo casi siempre pago.
Nunca bebo vino o cerveza durante las horas de trabajo.
Paso tiempo con mi familia y amigos y cocino muy bien.

## Colores favoritos

Mis colores favoritos son rojo, azul y negro.

## Mi rutina diaria

Normalmente, yo me despierto a las seit de la manana y me levanto en seguida.
No me gusta acostarme en la cama por much tiempo.
No corro por la mañana.
Me lavo a las siete y dieciséis.

### LUNES

El lunes por la mañana a las siete y media me tomo un café.
Yo y mi mujer estudiamos español a las siete de la tarde.

### MARTES

El martes y jueves por la noche a las nueve yo juego al tenis.
Normalmente, disputes de comer a las diez por la noche leo un libro.
Por la noche a las once menos cuarto yo me acuesto.

### MIÉRCOLES

El miércoles y viernes a las media día voy a la piscina y después a la dos y cuarto yo descanso un poquito.

### JUEVES

El jueves a las dos y media como una hamburguesa si mi mujer cocina.
A las ocho y media de un paseo por el parque cuando hace fresco.

### VIERNES

Yo y mi mujer estudiamos español a las nueve de la mañana.
El viernes a las diez yo desayuno dos huevos fritos si mi mujer cocina y por la noche a las diaz veo series de
televisión.

### SÁBADO

Yo y mi mujer estudiamos español a las nueve de la mañana.
El viernes a las diez yo desayuno dos huevos fritos si mi mujer cocina y por la noche a las diaz veo series de
televisión.

### DOMINGO

El domingo a las once y media viajamos en coche fuera de la ciudad o damos un paseo por el centro de la ciudad.