# Tener

To have

## Conjugación

### Presente

| singular              | plural                       |
|-----------------------|------------------------------|
| yo tengo              | nosotros tenemos             |
| tú tienes             | vosotros tenéis              |
| él, ella, usted tiene | ellos, ellas, ustedes tienen |
| vos te tenés          |                              |

### Ejemplo

Tengo la camisa negra.
Tengo treinta y cuatro años
