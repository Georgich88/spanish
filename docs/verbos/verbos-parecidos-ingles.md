# Los verbos españoles parecidos al inglés

| español                    | inglés            |
|----------------------------|-------------------|
| aplaudir                   | to applaud        |
| b                          |                   |
| c                          |                   |
| ch                         |                   |
| debatir                    | to debate         |
| decidir                    | to decide         |
| dividir                    | to divide         |
| e                          |                   |
| f                          |                   |
| g                          |                   |
| h                          |                   |
| i                          |                   |
| j                          |                   |
| k                          |                   |
| l                          |                   |
| ll                         |                   |
| mover                      | to move           |
| n                          |                   |
| ñ                          |                   |
| o                          |                   |
| pasar                      | to pass, to spend |
| q                          |                   |
| reparar                    | to repair         |
| rehusar                    | to refuse         |
| servir                     | to serve          |
| [sufrir](dict/s/sufrir.md) | to suffer         |
| t                          |                   |
| usar                       | to use            |
| v                          |                   |
| w                          |                   |
| x                          |                   |
| y                          |                   |
| z                          |                   |