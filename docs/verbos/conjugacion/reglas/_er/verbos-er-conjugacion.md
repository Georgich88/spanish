# Los verbos que terminan en `-ER`

## Conjugación

### Presente

| singular             | plural                      |
|----------------------|-----------------------------|
| yo `-o`              | nosotros `-emos`            |
| tú `-es`             | vosotros `-éis`             |
| él, ella, usted `-e` | ellos, ellas, ustedes `-en` |
| vos `-és`            |                             |

Beber

| singular             | plural                      |
|----------------------|-----------------------------|
| yo bebo              | nosotros bebemos            |
| tú bebes             | vosotros bebéis             |
| él, ella, usted bebe | ellos, ellas, ustedes beben |
| vos te bebés         |                             |

### Ejemplo

1. Raúl lee muchos libros históricos.
1. Osvaldo y Pedro me deben mucho dinero.
1. Daniel, eres muy delgado, ¿es que no comes nada?
1. Yo no comprendo nada de política.
1. Mi amiga y yo aprendemos italiano juntas.
1. Tu perro siempre escondes su comida.